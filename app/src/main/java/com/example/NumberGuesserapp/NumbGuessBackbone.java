package com.example.NumberGuesserapp;

import java.util.ArrayList;

public class NumbGuessBackbone {
    public static int[][] GenCards(int maxnum) {
        int arraylength = Integer.toBinaryString(maxnum).length();
        ArrayList<ArrayList<Integer>> cardArrayList = new ArrayList<ArrayList<Integer>>();
        for (int i = 0; i < arraylength; ++i) {
            cardArrayList.add(new ArrayList<Integer>());
        };
        for (int n = 1; n <= maxnum; n++) {
            String nbin = Integer.toBinaryString(n);
            for (int i = nbin.length() - 1; i >= 0; i--){
                if (nbin.charAt(i) == '1') {
                    cardArrayList.get(nbin.length() - (i + 1)).add(n);
                };
            };
        };
        int[][] cardlist = new int[arraylength][];
        for (int i = 0; i < arraylength; i++) {
            int axislength = cardArrayList.get(i).size();
            cardlist[i] = new int[axislength];
            for (int n = 0; n < axislength; n++) {
                cardlist[i][n] = cardArrayList.get(i).get(n);
            };
            cardArrayList.set(i, null);
        };
        cardArrayList = null;
        return cardlist;
    };
    public static int CalcGuess(int maxnum, int[] idlist) {
        int num = 0;
        for (int i = 0; i < idlist.length; i++) {
            num += Math.pow(2, idlist[i] - 1);
        };
        if (num > maxnum) {
            return num;
        } else {
            return -1;
        }
    };
//    public static void main(String[] args) {
//        int inp = 30;
//        int[] guesslist = {1,2,3};
//        if (args.length > 0) {
//            inp = Integer.parseInt(args[0]);
//        };
//        int[][] out = GenCards(inp);
//        for (int i = 0; i < out.length; i++) {
//            System.out.println(Arrays.toString(out[i]));
//        };
//        System.out.println(CalcGuess(guesslist));
//    };
};
