package com.example.NumberGuesserapp;

public class Speicher {
    int[][] cards;
    int finalGuess;

    private static final Speicher OBJ = new Speicher();

    private Speicher() {

    }
    public static Speicher getInstance() {
        return OBJ;
    }
    public int[][] getCards() {
        return cards;
    }
    public void setCards(int[][] cards) {
        this.cards = cards;
    }

    public int getFinalGuess() {
        return finalGuess;
    }
    public void setFinalGuess(int finalGuess) {
        this.finalGuess = finalGuess;
    }
}