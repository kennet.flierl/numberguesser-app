package com.example.NumberGuesserapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

public class Tutorial extends AppCompatActivity {
    int ttstp = 1;
    Button nxtstp;
    Button bckstp;
    TextView stp;

    public void checkTtStep(){
        if(ttstp == 1) {
            stp.setText("First you Set a range and come up with a Number in that range.");
        }
        if(ttstp == 2) {
            stp.setText("After you click start, you will be given number groups.");
        }
        if(ttstp == 3) {
            stp.setText("Select the Groups with your number and click guess.");
        }
        if(ttstp == 4) {
            stp.setText("Have fun!");
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial);
        nxtstp = findViewById(R.id.nextstepintt);
        bckstp = findViewById(R.id.backstepintt);
        stp = findViewById(R.id.ttstep);
        checkTtStep();
        nxtstp.setOnClickListener(view -> {
            if (ttstp < 4) {
                ttstp++;
                checkTtStep();
            };
        });
        bckstp.setOnClickListener(view -> {
            if (ttstp > 1) {
                ttstp--;
                checkTtStep();
            };
        });

    }
}